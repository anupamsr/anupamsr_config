This repository holds all the config files that I need whenever I use a new
computer. You can use them as you like.

LICENSE:
To the extent possible under law, Anupam Srivastava has waived all copyright
and related or neighboring rights to this work. This work is published from:
India.
http://creativecommons.org/publicdomain/zero/1.0/
