#!/bin/env zsh
set +x

MOUNT_PATH=/mnt/ext

COLOR_RED=$(tput setaf 1)
COLOR_NORMAL=$(tput sgr0)
COLOR_GREEN=$(tput setaf 2)

printf "Starting at %s\n" "$(date)"
problem=false
while true; do
    count=$(mount|grep -c ${MOUNT_PATH})
    out=$(ls ${MOUNT_PATH} 2>&1)
    if [[ $? -ne 0 || $count -ne 1 ]]; then
        if ! $problem; then
            printf "%bFailed at %s %s%b\n" "${COLOR_RED}" "$(date)" "$out" "${COLOR_NORMAL}"
            problem=true
        else
            printf "\rWaiting...%s" "$(date)"
        fi
    elif $problem; then
        printf "\n%bOnline at %s%b\n" "${COLOR_GREEN}" "$(date)" "${COLOR_NORMAL}"
        problem=false
    fi
    sleep 1
done
