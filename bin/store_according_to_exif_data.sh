#!/usr/bin/env bash

if [[ ${#} -lt 1 ]]; then
    echo "Usage: ${0} image1 image2 ..."
    exit 1
fi

cat <<-EOF
This program "copies" the images into new directories. Make sure everything
is correct before deleting the original images.
EOF
read -r

check_if_exists () {
    echo -n "Checking for ${1}... "
    if ! command -v "${1}"; then
        echo "missing."
        return 1
    fi
    return 0
}

cmd=""

# This sections finds a valid program, otherwise it exits
if ! check_if_exists exiftime; then
    if ! check_if_exists jhead; then
        if ! check_if_exists exif; then
            if ! check_if_exists identify; then
                echo "No usable program found to extract exif tags. Please install" >&2
                echo "exiftags/jhead/GraphicsMagick in decreasing order of preference" >&2
                exit 1
            else
                cmd="identify -format '%[EXIF:DateTimeOriginal]'"
                args="|cut -d ' ' -f1|sed -e 's/:/_/g'"
            fi
        else
            cmd="exif"
            args="|grep 'Date and Time'|grep Orig|sed -e 's/|/ /g' -e 's/:/_/g'|cut -d ' ' -f5"
        fi
    else
        cmd="jhead -exifmap"
        args="|grep Date/Time|cut -d ' ' -f6|sed -e 's/:/_/g'"
    fi
else
    cmd="exiftime"
    args="|grep Generated|cut -d ' ' -f3|sed -e 's/:/_/g'"
fi

# This section moves the images into corresponding directories
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for image in "${@}"; do
    dirtostore=$(eval "${cmd}" "${image}" "${args}")
    mkdir -p "${dirtostore}"
    cp -v "${image}" "${dirtostore}"
done
IFS=$SAVEIFS

