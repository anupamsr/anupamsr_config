" Vim syntax file
" Language:	Mader input file
" Maintainer:	Anupam Srivastava <srivasta@itt.uni-stuttgart.de>
" Last Change:	$Id: mader.vim 36 2007-08-27 16:39:23Z srivasta $

" Derived from BASIC.vim and FORTRAN.vim syntax file from original vim
"
" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn case ignore

" Known keywords
syn keyword maderStatement	Restart eps kB_Ref mRef sigmaRef AnzKomp NKomp rho T P NPT VMasse dt rc EquiNVTSchritte EquiNPTSchritte SimSchritte BoxRatio Output Scheibenzahl Density Limit Film BilderMax FilmAb OutputVis MaxAnzSites MaxAnzLadungen MaxAnzDipole MaxAnzQuadrupole eps_ReactionField eta xi AnzSites AnzLadungen AnzDipole AnzQuadrupole rSite kB m sig I_Dummy Farbe eps x y z rLadung abs mLadung rDipol eMy absMy rQuadrupol eQ absQ absLadung HasWall Wall position
syn keyword maderTodo 		contained	TODO
syn match   maderBoolean        "\.\s*\(TRUE\|FALSE\)\s*\."
syn keyword maderTypeSpecifier  K atomare Masseneinheit Angstroem mol l reduziert fs MPa wieviele Protonenladung
	
"Numbers of various sorts
" Integers
syn match   maderNumber		display "\<\d\+\(_\a\w*\)\=\>"
" floating point number, without a decimal point
syn match   maderFloat		display "\<\d\+[deq][-+]\=\d\+\(_\a\w*\)\=\>"
" floating point number, starting with a decimal point
syn match   maderFloat		display "\.\d\+\([deq][-+]\=\d\+\)\=\(_\a\w*\)\=\>"
" floating point number, no digits after decimal
syn match   maderFloat		display "\<\d\+\.\([deq][-+]\=\d\+\)\=\(_\a\w*\)\=\>"
" floating point number, D or Q exponents
syn match   maderFloat		display "\<\d\+\.\d\+\([dq][-+]\=\d\+\)\=\(_\a\w*\)\=\>"
" floating point number
syn match   maderFloat		display "\<\d\+\.\d\+\(e[-+]\=\d\+\)\=\(_\a\w*\)\=\>"

"starting sentence
syn match   maderInfoSign	display "!\s\+>"
syn region  maderInfo		start="($Id" end=+$)+ end="$"

"numbers should not have ',' they should have '.'
syn match   maderError		display "\,\d\+\([deq][-+]\=\d\+\)\=\(_\a\w*\)\=\>"
syn match   maderError		display "\<\d\+\,\([deq][-+]\=\d\+\)\=\(_\a\w*\)\=\>"
syn match   maderError		display "\<\d\+\,\d\+\([dq][-+]\=\d\+\)\=\(_\a\w*\)\=\>"
syn match   maderError		display "\<\d\+\,\d\+\(e[-+]\=\d\+\)\=\(_\a\w*\)\=\>"

syn region  maderBracket	start="(" end=+)+ contains=maderError

syn region  maderComment	start="!" end="$" contains=maderTodo,maderInfo,maderInfoSign
syn region  maderComment	start="#" end="$" contains=maderTodo,maderInfo,maderInfoSign

syn match   maderMathsOperator   "-\|=\|[:<>+\*^/\\]"
syn match   maderMathsOperator   "\.\s*\(AND\|OR\)\s*\."

" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_mader_syntax_inits")
  if version < 508
    let did_mader_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink maderNumber		Number
  HiLink maderError		Error
  HiLink maderStatement		Statement
  HiLink maderComment		Comment
  HiLink maderTodo		Todo
  HiLink maderFunction		Identifier
  HiLink maderTypeSpecifier	Type
  HiLink maderMathsOperator 	Special
  HiLink maderBoolean		Boolean
  HiLink maderBracket		Type
  HiLink maderInfo		PreProc
  HiLink maderInfoSign		Todo
  delcommand HiLink
endif

let b:current_syntax = "mader"

" vim: ts=8
