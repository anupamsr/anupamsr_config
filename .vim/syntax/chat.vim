" Vim syntax file
" Language:	Chat history file
" Maintainer:	Anupam Srivastava <srivasta@itt.uni-stuttgart.de>
" Last Change:	$Id$

" Derived from MADER.vim
"
" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syntax spell toplevel

syn case ignore

" Known keywords

syn match   chatEmoticon	":[<>()pPD]\+"
syn region  chatItalic		start="_" end="_" end="$"
syn region  chatBold		start="*" end="*" end="$"
syn region  chatName		start="^" end=":" end="$"
syn region  chatBubble		start="(" end=")" end="$"

" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_chat_syntax_inits")
  if version < 508
    let did_chat_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink chatItalic			PreProc
  HiLink chatBold			Todo
  HiLink chatName			Statement
  HiLink chatEmoticon		Type
  HiLink chatBubble			Comment
  delcommand HiLink
endif

let b:current_syntax = "chat"

" vim: ts=8
