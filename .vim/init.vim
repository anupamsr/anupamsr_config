" References:
" 1) http://vim.wikia.com/wiki/User:Tonymec/vimrc
" 2) http://items.sjbach.com/319/configuring-vim-right
" 3) /usr/share/vim/vim72/vimrc_example.vim
" 4) http://vim.wikia.com/wiki/Align_endif_with_corresponding_if_or_ifdef_directive
" 5) http://vim.wikia.com/wiki/C%2B%2B_code_completion
" 6) http://vim.wikia.com/wiki/VimTip810
" 7) http://www.mingw.org/faq/How_do_I_enable_colour_in_msys_Vim_when_using_mintty

" Forget about Vi
set nocompatible

" Disable digraph
" Annoying feature for entering Unicode characters
set nodigraph

" Don't unload files but just hide them
" Disabled because we use many times multiple terminals
"set hidden

" Show partial commands in status line
set showcmd

" Enable command line completion in enhanced mode
set wildmenu

" Don't convert tab to spaces
set noexpandtab

" Keeps cursor in same column on some commands
set nostartofline

" Show row and column numbers on status line
set ruler

" Allows backspace to work everywhere
set backspace=2

" Set formatting options (for comments, paragraphs etc.); works with
" autoindent
set formatoptions=cqrtn

" Show status bar always
set laststatus=2

" Shortens all the messages
set shortmess=atI

" Allows wrap only when cursor keys are used
set whichwrap=<,>,[,]

" Tab length is 4 characters
set tabstop=4

" Indent with 4 spaces
set shiftwidth=4

" Define what starts a comment (no use here because I compiled with +comments)
set comments=s1:/*,mb:*,ex:*/,://,b:#

" Don't pause when screen is filled
" Disabled because editing large files
"set nomore

" Store temporary files in a central spot
set backupdir=~/vim_backup,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/vim_backup,~/.tmp,~/tmp,/var/tmp,/tmp

" Strings to load in List mode
set listchars=tab:>-,trail:.,eol:$,extends:>
nmap <silent> <leader>s :set nolist!<CR>

" Maintain more context around the cursor
set scrolloff=3

" current directory is always matching the content of the active window
" Disabled because it is troublesome while vimdiffing
"set autochdir

" when splitting, put new window after current by default
if exists("+splitbelow")
    set splitbelow
endif
if exists("+splitright")
    set splitright
endif

" From 'emerge latextsuite' messages
set grepprg=grep\ -nH\ $*

" Press F5 to toggle through paste mode
set pastetoggle=<F5>

" Enable spell-check
" Disabled because gets annoying when default
"setlocal spell

" Do not warn (ring bells) on errors
"set noerrorbells

" Disable beep (audible bell)
"set t_vb=

" Show visual bell
set visualbell

" Do incremental search
set incsearch

" Always show line numbers
set number

" Set default encoding to unicode
set encoding=utf-8

if has("vms")
    set nobackup    " do not keep a backup file, use versions instead
else
    set backup      " keep a backup file
endif

if has("mouse")
    " Enable mouse everywhere
    set mouse=a
    set mousemodel=popup_setpos "See help
endif

if has("viminfo")
    " Defines what to store in .viminfo
    set viminfo=%,'50,\"50,:50
    set history=500
endif

if &term == "xterm-256color" || &term == "rxvt-unicode" || &term == "xterm"
\ || &term == "Eterm" || &term == "mlterm" || &term == "screen"
\ || &term == "screen-256color" || &term == "tmux"
    " enable 256 colors (for terminals that support it - xterm needs
    " ./configure options, urxvt needs patch and screen needs .screenrc
    " modification)
	"set termguicolors
	colorscheme 256_asu1dark
else
	colorscheme peaksea
endif

" Change this when you have figured out how to use screen-it for tmux
if &term == "screen"
    " This will be true for tmux as well as screen
    set ttyfast
    map <Esc>OH <Home>
    map! <Esc>OH <Home>
    map <Esc>OF <End>
    map! <Esc>OF <End>
endif

" Set background to dark since we use dark mintty without launching X
set background=dark

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

if has("autocmd")
    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
      \ if line("'\"") > 1 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif

    autocmd FileType c,cpp,slang set cindent
    autocmd FileType c,cpp,slang set noexpandtab
    autocmd FileType perl set expandtab

    filetype plugin indent on
else
    " always set autoindenting on
    set autoindent
endif " has("autocmd")

" Convenient command to see the difference between the current bugger and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
            \ | wincmd p | diffthis
endif

ino <Down> <C-O>gj
ino <Up> <C-O>gk
nno <Down> gj
nno <Up> gk

" F2 highlights search string
nno <F2> :set hls!<bar>set hls?<CR>

" F3 disables sytax highlighting
"nno <F3> :syn clear<CR>

" F4 enables line-numbers
nno <F4> :set nu!<bar>set nu?<CR>
nno <F3> :diffget<CR>
nno <F6> :diffput<CR>

" For german layout
nnoremap ü <C-]>
nnoremap ö <c-O>

if has("gui_running")
    set guifont=DejaVu\ Sans\ Mono\ 8
endif

" Align endif with corresponding if or ifdef directive
inoremap #en #endif<Esc>bbd0%y0<C-o>0PA

" C++ code completion
" configure tags - add additional tags here or comment out not-used ones
set tags+=~/.vim/tags/cpp
set tags+=~/.vim/tags/gl
set tags+=~/.vim/tags/sdl
set tags+=~/.vim/tags/qt4
" build tags of your own project with Ctrl-F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

" Highlight long lines
set textwidth=78

" Enable color support for msys
"let &t_Co=256
"let &t_AF="\e[38;5;%dm"
"let &t_AB="\e[48;5;%dm"

" PP Specific
set path+=$BUILDTOP

" Treat oml and mdf as xml
au BufRead,BufNewFile *.oml set filetype=xml
au BufRead,BufNewFile *.mdf set filetype=xml

" Add highlighting for function definition in C++
function! EnhanceCppSyntax()
  syn match cppFuncDef "::\~\?\zs\h\w*\ze([^)]*\()\s*\(const\)\?\)\?$"
  hi def link cppFuncDef Special
endfunction
autocmd Syntax cpp call EnhanceCppSyntax()

" Enable FSwitch
au! BufEnter *.cpp let b:fswitchdst = 'hpp,h' | let b:fswitchlocs = '../inc'

" Enable Go
set rtp+=$GOROOT/misc/vim
