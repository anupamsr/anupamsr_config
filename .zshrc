# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

add_path () {
    if ! (($path[(Ie)$1])); then
        path=( "$1" "${path[@]}" )
    fi
}

# Rust/Cargo
add_path "$HOME/.cargo/bin"

# Python/Pip
add_path "$HOME/.local/bin"

# Go
add_path "$HOME/go/bin"

# Java
add_path "$JAVA_HOME/bin"

add_path "$HOME/bin"
path=( $^path(N) )

setopt re_match_pcre   # _fix-omz-plugin function uses this regex style

# Workaround for zinit issue#504: remove subversion dependency. Function clones all files in plugin
# directory (on github) that might be useful to zinit snippet directory. Should only be invoked
# via zinit atclone"_fix-omz-plugin"
_fix-omz-plugin() {
    if [[ ! -f ._zinit/teleid ]] then return 0; fi
    if [[ ! $(cat ._zinit/teleid) =~ "^OMZP::.*" ]] then return 0; fi
    local OMZP_NAME=$(cat ._zinit/teleid | sed -n 's/OMZP:://p')
    git clone --quiet --no-checkout --depth=1 --filter=tree:0 https://github.com/ohmyzsh/ohmyzsh
    cd ohmyzsh
    git sparse-checkout set --no-cone plugins/$OMZP_NAME
    git checkout --quiet
    cd ..
    local OMZP_PATH="ohmyzsh/plugins/$OMZP_NAME"
    local file
    for file in ohmyzsh/plugins/$OMZP_NAME/*~(.gitignore|*.plugin.zsh)(D); do
        local filename="${file:t}"
        echo "Copying $file to $(pwd)/$filename..."
        cp $file $filename
    done
    rm -rf ohmyzsh
}

ZINIT_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/zinit/zinit.git"
source "$ZINIT_HOME/zinit.zsh"
zinit trackbinds light-mode depth'1' for \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-binary-symlink \
    zdharma-continuum/zinit-annex-patch-dl \
    OMZL::compfix.zsh \
    OMZL::completion.zsh \
    OMZL::correction.zsh \
    OMZL::grep.zsh \
    OMZL::history.zsh \
    OMZL::key-bindings.zsh \
    OMZL::theme-and-appearance.zsh \
    as'completion' atpull'zinit cclear' blockf zsh-users/zsh-completions \
    as'program' from'gh-r' atclone'cp -vf man/**/zoxide*.1 $ZPFX/man/man1; ./zoxide init zsh > init.zsh' atpull'%atclone' src'init.zsh' nocompile'!' ajeetdsouza/zoxide \
    as'program' from'gh-r' lbin'!**/bat' nocompile @sharkdp/bat \
    as'program' from'gh-r' lbin'!**/delta' nocompile dandavison/delta \
    as'program' from'gh-r' lbin'!**/fd' atclone'cp -vf **/*.1 $ZPFX/man/man1' nocompile @sharkdp/fd \
    as'program' from'gh-r' lbin'!**/immich-go' nocompile simulot/immich-go \
    as'program' pick'bin/git-fuzzy' bigH/git-fuzzy \
    atinit'typeset -gA FAST_HIGHLIGHT; FAST_HIGHLIGHT[git-cmsg-len]=100; zpcompinit; zpcdreplay;' zdharma-continuum/fast-syntax-highlighting \
    djui/alias-tips \
    https://raw.githubusercontent.com/anupamsr/hhighlighter/master/h.sh \
    jeffreytse/zsh-vi-mode \
    romkatv/powerlevel10k \
    atpull'%atclone' atclone'_fix-omz-plugin' OMZP::tmux \
    wbingli/zsh-wakatime \
    zsh-users/zsh-autosuggestions \
    pack ls_colors
zinit wait'1' lucid trackbinds light-mode depth'1' for \
    zdharma-continuum/history-search-multi-word \
    bindmap'^R -> ^S' pack'bgn+keys' fzf
zinit wait'1' lucid trackbinds light-mode depth'1' for \
    atload'zstyle ":completion:*" list-colors "${(s.:.)LS_COLORS}"
           zstyle ":completion:*:descriptions" format "[%d]"
           zstyle ":fzf-tab:*" fzf-command ftb-tmux-popup' Aloxaf/fzf-tab
eval "$(zoxide init zsh)"
unalias zi &>/dev/null
if neovide_loc="$(command -v neovide)" && [[ -x "$neovide_loc" ]]; then
    alias nv="neovide --multigrid"
fi
if ag_loc="$(command -v ag)" && [[ -x "$ag_loc" ]]; then
    alias ag="ag --color-path 35 --color-match '1;31' --color-line-number 32"
fi
alias gf='git fuzzy'
alias vcat=vimcat
alias dcd='docker compose down --remove-orphans'
alias dcuf='docker compose pull && docker compose up -d && docker compose logs -f'
alias -s json='jq -r .'
alias -s md=glow
alias -s xml=bat
alias -s yaml=bat
alias -s yml=bat

setopt histignorealldups
setopt histfindnodups
export HISTFILESIZE=10000000
export HISTSIZE=10000000
export SAVEHIST=10000000
export VISUAL=vim
export EDITOR=vim
export GPG_TTY=$TTY
export PAGER=vimpager
export BAT_PAGER='less -RF'
export DELTA_PAGER='less -RF'
export FZF_DEFAULT_COMMAND="fd -utf --color=never"
export FZF_DEFAULT_OPTS=""
export FZF_CTRL_T_COMMAND="${FZF_DEFAULT_COMMAND}"
export FZF_CTRL_T_OPTS="${FZF_DEFAULT_OPTS}"
export FZF_ALT_C_COMMAND="fd -utd --color=never"
export FZF_ALT_C_OPTS="${FZF_DEFAULT_OPTS}"

autoload -Uz zkbd up-line-or-beginning-search down-line-or-beginning-search
bindkey -v
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
source "$HOME/.zkbd/$TERM-${DISPLAY:-$VENDOR-$OSTYPE}"
bindkey "${key[Backspace]}" vi-backward-delete-char
bindkey "${key[Insert]}" vi-yank
bindkey "${key[Home]}" vi-beginning-of-line
bindkey "${key[Delete]}" vi-delete-char
bindkey "${key[End]}" vi-end-of-line
bindkey "${key[Left]}" vi-backward-char
bindkey "${key[Right]}" vi-forward-char
bindkey "${key[Up]}" up-line-or-beginning-search
bindkey "${key[Down]}" down-line-or-beginning-search
bindkey "${key[PageUp]}" history-beginning-search-backward
bindkey "${key[PageDown]}" history-beginning-search-forward

source "$HOME/.p10k.zsh"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
