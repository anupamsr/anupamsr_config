" References:
" 1) http://vim.wikia.com/wiki/User:Tonymec/vimrc
" 2) http://items.sjbach.com/319/configuring-vim-right
" 3) /usr/share/vim/vim72/vimrc_example.vim
" 4) http://vim.wikia.com/wiki/Align_endif_with_corresponding_if_or_ifdef_directive
" 5) http://vim.wikia.com/wiki/C%2B%2B_code_completion
" 6) http://vim.wikia.com/wiki/VimTip810
" 7) http://www.mingw.org/faq/How_do_I_enable_colour_in_msys_Vim_when_using_mintty
" 8) https://github.com/neoclide/coc.nvim
" 9) https://bluz71.github.io/2018/12/04/fuzzy-finding-in-vim-with-fzf.html

" Forget about Vi
set nocompatible

" Remap leader key to ,
let g:mapleader=','

" Disable digraph
" Annoying feature for entering Unicode characters
set nodigraph

if !has("nvim")
    " Function keys that start with an <Esc> are recognized in Insert
    set esckeys

    " Set internal encoding of vim, not needed on neovim, since coc.nvim using some
    " unicode characters in the file autoload/float.vim
    set encoding=utf-8
endif

" Don't unload files but just hide them
" Disabled because we use many times multiple terminals
"set hidden

" Show partial commands in status line
set showcmd

" Enable command line completion in enhanced mode
set wildmenu

" Convert tab to spaces
set expandtab

" Keeps cursor in same column on some commands
set nostartofline

" Show row and column numbers on status line
set ruler

" Allows backspace to work everywhere
set backspace=2

" Set formatting options (for comments, paragraphs etc.); works with
" autoindent
set formatoptions=cqrtn

" Show status bar always
set laststatus=2

" Shortens all the messages
set shortmess=atI

" Allows wrap only when cursor keys are used
set whichwrap=<,>,[,]

" Tab length is 4 characters
set tabstop=4

" Indent with 4 spaces
set shiftwidth=4

" Don't pause when screen is filled
" Disabled because editing large files
"set nomore

" Don't update buffer with content changed outside of vim
set noautoread

" Store temporary files in a central spot
set backupdir=$HOME/vim_backup,$HOME/.tmp,$HOME/tmp,/var/tmp,/tmp
set directory=$HOME/vim_backup,$HOME/.tmp,$HOME/tmp,/var/tmp,/tmp

" Strings to load in List mode
set listchars=tab:>-,trail:.,eol:$,extends:>
nmap <silent> <leader>s :set nolist!<CR>

" Maintain more context around the cursor
set scrolloff=3

" current directory is always matching the content of the active window
" Disabled because it is troublesome while vimdiffing
"set autochdir

" when splitting, put new window after current by default
if exists("+splitbelow")
    set splitbelow
endif
if exists("+splitright")
    set splitright
endif

" Press F5 to toggle through paste mode
if has("nvim")
    nnoremap <silent> <F5> :set paste!<cr>
    inoremap <silent> <F5> <Esc>:set paste!<cr>i
else
    set pastetoggle=<F5>
endif

" Enable spell-check
" Disabled because gets annoying when default
"setlocal spell

" Do not warn (ring bells) on errors
"set noerrorbells

" Disable beep (audible bell)
"set t_vb=

" Show visual bell
set visualbell

" Do incremental search
set incsearch

" Always show line numbers
set number

" Show relative line numbers too
set relativenumber

" Set default encoding to unicode
set encoding=utf-8

" Don't add <EOL> at the end of file if it is not present already
set nofixendofline

if has("vms")
    set nobackup    " do not keep a backup file, use versions instead
else
    set backup      " keep a backup file
endif

if has("mouse")
    " Enable mouse everywhere
    set mouse=a
    set mousemodel=popup_setpos "See help
    if !(has("nvim"))
        set balloonevalterm
    endif
endif

if has("viminfo")
    " Defines what to store in .viminfo
    set viminfo=%,'50,\"50,:50
    set history=500
endif

if !has("nvim")
    if &term == "alacritty" || &term == "tmux-256color"
        set ttymouse=sgr
    elseif &term == "rxvt-unicode" || &term == "rxvt-unicode-256color"
        set ttymouse=urxvt
    endif
endif

if has("autocmd")
    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
                \ if line("'\"") > 1 && line("'\"") <= line("$") |
                \   exe "normal! g`\"" |
                \ endif

    autocmd FileType c,cpp,slang set cindent

    filetype plugin indent on
else
    " always set autoindenting on
    set autoindent
endif " has("autocmd")

" Convenient command to see the difference between the current bugger and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

ino <Down> <C-O>gj
ino <Up> <C-O>gk
nno <Down> gj
nno <Up> gk

" F2 highlights search string
nno <F2> :set hls!<bar>set hls?<CR>

" F3 disables sytax highlighting
"nno <F3> :syn clear<CR>

" F4 enables line-numbers
nno <F4> :set nu!<bar>set nu?<CR>
nno <F3> :diffget<CR>
nno <F6> :diffput<CR>

" Align endif with corresponding if or ifdef directive
inoremap #en #endif<Esc>bbd0%y0<C-o>0PA

" C++ code completion
" configure tags - add additional tags here or comment out not-used ones
set tags+=$HOME/.vim/tags/cpp
set tags+=$HOME/.vim/tags/gl
set tags+=$HOME/.vim/tags/sdl
set tags+=$HOME/.vim/tags/qt4
" build tags of your own project with Ctrl-F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=menuone,menu,longest,preview

" Highlight long lines
set wrap
set linebreak

" But don't insert new lines
set textwidth=0
set wrapmargin=0

" Add highlighting for function definition in C++
function! EnhanceCppSyntax()
    syn match cppFuncDef "::\~\?\zs\h\w*\ze([^)]*\()\s*\(const\)\?\)\?$"
    hi def link cppFuncDef Special
endfunction
autocmd Syntax cpp call EnhanceCppSyntax()

set ttyfast
syntax on
set hlsearch

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=number

if (has("termguicolors"))
    " From https://sw.kovidgoyal.net/kitty/faq/#kitty-is-not-able-to-use-my-favorite-font
    " Styled and colored underline support
    let &t_AU = "\e[58:5:%dm"
    let &t_8u = "\e[58:2:%lu:%lu:%lum"
    let &t_Us = "\e[4:2m"
    let &t_Cs = "\e[4:3m"
    let &t_ds = "\e[4:4m"
    let &t_Ds = "\e[4:5m"
    let &t_Ce = "\e[4:0m"
    " Strikethrough
    let &t_Ts = "\e[9m"
    let &t_Te = "\e[29m"
    " Truecolor support
    let &t_8f = "\e[38:2:%lu:%lu:%lum"
    let &t_8b = "\e[48:2:%lu:%lu:%lum"
    let &t_RF = "\e]10;?\e\\"
    let &t_RB = "\e]11;?\e\\"
    set termguicolors
    " Bracketed paste
    let &t_BE = "\e[?2004h"
    let &t_BD = "\e[?2004l"
    let &t_PS = "\e[200~"
    let &t_PE = "\e[201~"
    " Cursor control
    let &t_RC = "\e[?12$p"
    let &t_SH = "\e[%d q"
    let &t_RS = "\eP$q q\e\\"
    let &t_SI = "\e[5 q"
    let &t_SR = "\e[3 q"
    let &t_EI = "\e[1 q"
    let &t_VS = "\e[?12l"
    " Focus tracking
    let &t_fe = "\e[?1004h"
    let &t_fd = "\e[?1004l"
    if !(has("nvim"))
        execute "set <FocusGained>=\<Esc>[I"
        execute "set <FocusLost>=\<Esc>[O"
    endif
    " Window title
    let &t_ST = "\e[22;2t"
    let &t_RT = "\e[23;2t"

    " vim hardcodes background color erase even if the terminfo file does
    " not contain bce. This causes incorrect background rendering when
    " using a color theme with a background color in terminals such as
    " kitty that do not support background color erase.
    let &t_ut=''
endif

" Find the desired VimPlug install location for different system configurations.
if(has('win32') || has('win64'))
    if has('nvim')
        let shareDir=$HOME.'\AppData\Local\nvim'
        let plugVim=shareDir.'\autoload\plug.vim'
    else
        let shareDir=$HOME.'\vimfiles'
        let plugVim=shareDir.'\autoload\plug.vim'
    endif
else
    if has('nvim')
        let shareDir=$HOME.'/.local/share/nvim/site'
        let plugVim=shareDir.'/autoload/plug.vim'
    else
        let shareDir=$HOME.'/.vim'
        let plugVim=shareDir.'/autoload/plug.vim'
    endif
endif

" Url of the VimPlug script.
let plugUri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
if empty(glob(expand(plugVim)))
    if has('win32') || has('win64')
        " Make sure the autoload directory has been created.
        exec '!md '.shareDir.'\autoload'

        " Download VimPlug using PowerSHell.
        exec '!powershell -command Invoke-WebRequest -Uri "'.plugUri.'" -OutFile '.plugVim.'"'
    else
        " Download VimPlug using curl.
        exec '!curl -fLo '.plugVim.' --create-dirs '.plugUri
    endif

    " Automatically run PlugInstall command.
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

if (empty($XDG_DATA_HOME))
    call plug#begin($HOME . '/.local/share/vim/plugged')
else
    call plug#begin($XDG_DATA_HOME . '/vim/plugged')
endif

Plug 'chr4/nginx.vim'
Plug 'dense-analysis/ale'
Plug 'dstein64/nvim-scrollview', { 'branch': 'main' }
Plug 'fladson/vim-kitty'
Plug 'heavenshell/vim-jsdoc'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'luochen1990/rainbow'
Plug 'morhetz/gruvbox'
Plug 'nanotee/zoxide.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'rkitover/vimpager'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'wakatime/vim-wakatime'
Plug 'zdharma-continuum/zinit-vim-syntax'
Plug 'chrisbra/csv.vim'

" telescope-zoxide
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'jvgrootveld/telescope-zoxide'

" vim-signify
if has('nvim') || has('patch-8.0.902')
  Plug 'mhinz/vim-signify'
else
  Plug 'mhinz/vim-signify', { 'tag': 'legacy' }
endif
call plug#end()

let g:deoplete#enable_at_startup = 1
let g:better_whitespace_enabled=1
let g:rainbow_active = 0 "Disables bold/italic etc. hence inactive. Use RainbowToggleOn to turn it on.
" Kitty - auto update kitty instance with updated config file
autocmd bufwritepost ~/.config/kitty/kitty.conf :silent !pkill -USR1 -f $(which kitty)

" Escape inside a FZF terminal window should exit the terminal window
" rather than going into the terminal's normal mode.
autocmd FileType fzf tnoremap <buffer> <Esc> <Esc>
nnoremap <silent> <Space><Space> :Files<CR>
"nnoremap <C-p> :Files<CR>
nnoremap <silent> <Space>. :Files <C-r>=expand("%:h")<CR>/<CR>
nnoremap <silent> <Space>b :Buffers<CR>
nnoremap <silent> <Space>g :GFiles<CR>
"nnoremap <silent> <Space>] :Tags<CR>
"nnoremap <silent> <Space>B :BTags<CR>
let g:fzf_commits_log_options = '--graph --color=always
            \ --format="%C(yellow)%h%C(red)%d%C(reset)
            \ - %C(bold green)(%ar)%C(reset) %s %C(blue)<%an>%C(reset)"'

nnoremap <silent> <Space>C :Commits<CR>
nnoremap <silent> <Space>c :BCommits<CR>
nnoremap <Space>/ :Rg<Space>

" try-catch for airline
try
    let g:airline_extensions = ['ale', 'branch', 'csv', 'fzf', 'hunks', 'quickfix', 'searchcount', 'tabline', 'whitespace']
    let g:airline#extensions#whitespace#checks = ['indent', 'trailing', 'long', 'mixed-indent-file', 'conflicts']
    let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
    let g:airline#extensions#tabline#show_buffers = 0
    let g:airline#extensions#tabline#tab_min_count = 2
    let g:airline_powerline_fonts = 1
    let g:airline_theme='gruvbox'
    let g:gruvbox_italic=1
    let g:gruvbox_contrast_dark='medium'
    let g:gruvbox_contrast_light='soft'
    autocmd vimenter * ++nested colorscheme gruvbox
    nmap <leader>1 <Plug>AirlineSelectTab1
    nmap <leader>2 <Plug>AirlineSelectTab2
    nmap <leader>3 <Plug>AirlineSelectTab3
    nmap <leader>4 <Plug>AirlineSelectTab4
    nmap <leader>5 <Plug>AirlineSelectTab5
    nmap <leader>6 <Plug>AirlineSelectTab6
    nmap <leader>7 <Plug>AirlineSelectTab7
    nmap <leader>8 <Plug>AirlineSelectTab8
    nmap <leader>9 <Plug>AirlineSelectTab9
    nmap <leader>0 <Plug>AirlineSelectTab0
    nmap <Tab> <Plug>AirlineSelectNextTab
    nmap <S-Tab> <Plug>AirlineSelectPrevTab
    noremap <C-t> :tabnew split<CR>
catch
    echo 'airline not installed.'
endtry
