set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc

let g:neovide_transparency=0.95
let g:neovide_remember_window_size=v:true
let g:neovide_cursor_vfx_mode = "railgun"
set guifont=AnonymousPro\ NF:h12
let g:python3_host_prog="/bin/python3"
let g:loaded_perl_provider = 0
