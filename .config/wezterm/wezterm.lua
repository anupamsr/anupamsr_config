local wezterm = require("wezterm")

local config = wezterm.config_builder()

-- https://github.com/protiumx/.dotfiles/blob/main/stow/wezterm/.config/wezterm/wezterm.lua
local process_icons = {
    ["docker"] = wezterm.nerdfonts.linux_docker,
    ["docker-compose"] = wezterm.nerdfonts.linux_docker,
    ["btm"] = "",
    ["psql"] = "󱤢",
    ["usql"] = "󱤢",
    ["kuberlr"] = wezterm.nerdfonts.linux_docker,
    ["ssh"] = wezterm.nerdfonts.fa_exchange,
    ["ssh-add"] = wezterm.nerdfonts.fa_exchange,
    ["kubectl"] = wezterm.nerdfonts.linux_docker,
    ["stern"] = wezterm.nerdfonts.linux_docker,
    ["nvim"] = wezterm.nerdfonts.custom_vim,
    ["vim"] = wezterm.nerdfonts.dev_vim,
    ["make"] = wezterm.nerdfonts.seti_makefile,
    ["node"] = wezterm.nerdfonts.mdi_hexagon,
    ["go"] = wezterm.nerdfonts.seti_go,
    ["python3"] = "",
    ["Python"] = "",
    ["zsh"] = wezterm.nerdfonts.dev_terminal,
    ["bash"] = wezterm.nerdfonts.cod_terminal_bash,
    ["htop"] = wezterm.nerdfonts.mdi_chart_donut_variant,
    ["cargo"] = wezterm.nerdfonts.dev_rust,
    ["sudo"] = wezterm.nerdfonts.fa_hashtag,
    ["lazydocker"] = wezterm.nerdfonts.linux_docker,
    ["git"] = wezterm.nerdfonts.dev_git,
    ["lua"] = wezterm.nerdfonts.seti_lua,
    ["wget"] = wezterm.nerdfonts.mdi_arrow_down_box,
    ["curl"] = wezterm.nerdfonts.mdi_flattr,
    ["gh"] = wezterm.nerdfonts.dev_github_badge,
    ["ruby"] = wezterm.nerdfonts.cod_ruby,
}

local function get_current_working_dir(tab)
    local current_dir = tab.active_pane and tab.active_pane.current_working_dir or { file_path = "" }
    local HOME_DIR = os.getenv("HOME")

    return current_dir.file_path == HOME_DIR and "~" or string.gsub(current_dir.file_path, "(.*[/\\])(.*)", "%2")
end

local function get_process(tab)
    if not tab.active_pane or tab.active_pane.foreground_process_name == "" then
        return nil
    end

    local process_name = string.gsub(tab.active_pane.foreground_process_name, "(.*[/\\])(.*)", "%2")
    if string.find(process_name, "kubectl") then
        process_name = "kubectl"
    end

    return process_icons[process_name] or string.format("[%s]", process_name)
end

function active_tab_idx(mux_win)
    for _, item in ipairs(mux_win:tabs_with_info()) do
        -- wezterm.log_info('idx: ', idx, 'tab:', item)
        if item.is_active then
            return item.index
        end
    end
end

wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
    local has_unseen_output = false
    if not tab.is_active then
        for _, pane in ipairs(tab.panes) do
            if pane.has_unseen_output then
                has_unseen_output = true
                break
            end
        end
    end

    local cwd = wezterm.format({
        { Text = get_current_working_dir(tab) },
    })

    local process = get_process(tab)
    local title = process and string.format(" %s (%s) ", process, cwd) or " [?] "

    if has_unseen_output then
        return {
            { Foreground = { Color = "#28719c" } },
            { Text = title },
        }
    end

    return {
        { Text = title },
    }
end)

wezterm.on("update-right-status", function(window, pane)
    local process = ""

    if state.debug_mode then
        local info = pane:get_foreground_process_info()
        if info then
            process = info.name
            for i = 2, #info.argv do
                process = info.argv[i]
            end
        end
    end

    local status = (#process > 0 and " | " or "")
    local name = window:active_key_table()
    if name then
        status = string.format("󰌌  { %s }", name)
    end

    if window:get_dimensions().is_full_screen then
        status = status .. wezterm.strftime(" %R ")
    end

    window:set_right_status(wezterm.format({
        { Foreground = { Color = "#7eb282" } },
        { Text = process },
        { Foreground = { Color = "#808080" } },
        { Text = status },
    }))
end)

config.adjust_window_size_when_changing_font_size = false
config.audible_bell = "Disabled"
config.font = wezterm.font("IosevkaTerm Nerd Font")
config.font_size = 12.0
config.color_scheme = "IR_Black"
config.window_background_opacity = 0.9
config.scrollback_lines = 10000000
config.enable_scroll_bar = true
config.enable_tab_bar = true
config.hide_tab_bar_if_only_one_tab = true
config.switch_to_last_active_tab_when_closing_tab = true
config.tab_bar_at_bottom = true
config.use_fancy_tab_bar = false
config.initial_cols = 150
config.initial_rows = 50
config.window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
}
config.colors = {
    cursor_bg = "#ff6d0a",
    cursor_fg = "black",
    cursor_border = "darkorange",
    compose_cursor = "orange",
    scrollbar_thumb = "darkgrey",
}
-- config.colors = {
--     foreground = "lightgrey",
--     background = "black",

--     -- Overrides the cell background color when the current cell is occupied by the
--     -- cursor and the cursor style is set to Block
--     cursor_bg = "orange",
--     -- Overrides the text color when the current cell is occupied by the cursor
--     -- cursor_fg = "black",
--     -- Specifies the border color of the cursor when the cursor style is set to Block,
--     -- or the color of the vertical or horizontal bar when the cursor style is set to
--     -- Bar or Underline.
--     cursor_border = "#ff6d0a",
--     compose_cursor = "#ff6d0a",
--     scrollbar_thumb = "darkgrey",
--     ansi = {
--         "black",
--         "red3",
--         "green3",
--         "#fdfa22",
--         "blue",
--         "magenta3",
--         "cyan3",
--         "gray90",
--     },
--     brights = {
--         "gray50",
--         "red",
--         "green",
--         "yellow",
--         "#4444ff",
--         "magenta",
--         "cyan",
--         "white",
--     },
-- }
config.default_cursor_style = "BlinkingBlock"
config.default_prog = { "zsh" }
config.canonicalize_pasted_newlines = "None"
config.inactive_pane_hsb = {
    saturation = 1.0,
    brightness = 0.6,
}
config.keys = {
    {
        key = "t",
        mods = "CTRL|SHIFT",
        -- https://github.com/wez/wezterm/issues/909
        action = wezterm.action_callback(function(win, pane)
            local mux_win = win:mux_window()
            local idx = active_tab_idx(mux_win)
            -- wezterm.log_info('active_tab_idx: ', idx)
            local tab = mux_win:spawn_tab({})
            -- wezterm.log_info('movetab: ', idx)
            win:perform_action(wezterm.action.MoveTab(idx + 1), pane)
        end),
    },
    {
        key = "_",
        mods = "CTRL|SHIFT|ALT",
        action = wezterm.action.SplitPane({
            direction = "Down",
        }),
    },
    {
        key = "|",
        mods = "CTRL|SHIFT|ALT",
        action = wezterm.action.SplitPane({
            direction = "Right",
        }),
    },
}
config.quick_select_patterns = {
    "[0-9a-f]{7,40}", -- hashes
    "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", -- uuids
    "https?:\\/\\/\\S+",
}
config.quote_dropped_files = "Posix"
config.window_decorations = "RESIZE"
config.window_frame = {
    active_titlebar_bg = config.colors.background,
    inactive_titlebar_bg = config.colors.background,
}

return config
